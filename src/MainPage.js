import React from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import Nav from './components/Nav';
import Loader from './components/Loader';


const MainPage = () => {
    const Landing = React.lazy(() => import('./views/Landing'));
    const Genres = React.lazy(() => import('./views/genres/Genres'));
    const Types = React.lazy(() => import('./views/types/Types'));
    const Books = React.lazy(() => import('./views/books/Books'));
    const Register = React.lazy(()=> import('./views/pages/Register'));
    const Login = React.lazy(()=> import('./views/pages/Login'));
    return (
        <React.Fragment>
            <Nav/>

        <BrowserRouter>
            <React.Suspense
            fallback = { Loader }
            >
                <Switch>
                    <Route 
                    path = {'/'}
                    exact
                    render = { props => <Landing { ...props }/>} />
                    <Route 
                    path = {'/genres'}
                    render = { props => <Genres { ...props }/>} />
                    <Route 
                    path = {'/types'}
                    render = { props => <Types { ...props }/>} />
                    <Route 
                    path = {'/fiction'}
                    render = { props => <Genres { ...props }/>} />
                    <Route 
                    path = {'/non-fiction'}
                    render = { props => <Genres { ...props }/>} />
                    <Route
                    path = {'/books'}
                    render = { props => <Books {...props }/>} />
                    <Route
                    path = {'/register'}
                    render = { props => <Register {...props}/>} />
                    <Route
                    path = {'/login'}
                    render = { props => <Login {...props}/>} />
                </Switch>
            </React.Suspense>
        
        
        
        </BrowserRouter>
            
        </React.Fragment>
      );
    }

export default MainPage;
