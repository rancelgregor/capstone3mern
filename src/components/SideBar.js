import React from 'react';
import BirdLogo from './bird.png';
import {Sidebar, SidebarImg, ButtonLink} from './StyledComponents';

const SideBar = props =>{
    return(
        <Sidebar>
            <div className="d-flex justify-content-end">
                <SidebarImg className="my-5" src={BirdLogo} alt="bird"/>
            </div>
            <div className ="d-flex flex-column align-items-center justify-content-center" style={{width: '100%'}}>
                 <div style={{width: '80%'}}>
                    <div className="text-center mb-3" style={{width: '100%', height: 'auto', border: '15px ridge rgba(64,109,21,0.65)'}}>
                        <h1 className="text-white" style={{textShadow: "-1px -1px 0 gray, 1px -1px 0 gray, -1px 1px 0 gray, 1px 1px 0 gray"}}>Welcome</h1>
                    </div>
                 </div>
                 <div style={{width: '100%', display: 'flex'}}>
                     <div style={{width: '90%'}}>
                     
                      {/* use map here */}

                    <div className="text-center my-1" style={{width: '110%', height: '50px', backgroundColor: 'rgba(10,107,20,0.7539390756302521', border: '2px solid black'}}>
                        <ButtonLink as ="a" href="/">
                            <h3 className="text-white py-1">{props.title}</h3>
                        </ButtonLink>
                    </div>


                    {/*  */}
                     </div>
                 </div>
            </div>
        </Sidebar>
    )
}

export default SideBar;