import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import MainPage from './MainPage';
import * as serviceWorker from './serviceWorker';
import SideBar from './components/SideBar';

// bootstrap
import 'bootstrap/dist/css/bootstrap.css';


ReactDOM.render(
  <React.StrictMode>
      <div style={{overflowY: 'hidden', height: '100vh'}}>
        <div >
          <SideBar>

          </SideBar>
        </div>

      <div>
      <MainPage/>
      </div>
      </div>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();