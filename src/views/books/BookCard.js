import React, {useState} from 'react';
import { Table, Button } from 'reactstrap';
import BookRent from './BookRent';

const BookCard = props => {

    const [toggleRent, setToggleRent] = useState(false);
    const [price, setPrice] = useState(undefined);

    const handleRent = (price) =>{
        setToggleRent(true);
        setPrice(price);

    }

    console.log(props.books);
    return(
        <>
            <div>
                <Table striped responsive>
                    <thead>
                        <tr className="text-center">
                            <th>#</th>
                            <th>Book Title</th>
                            <th>Book Author</th>
                            <th>Availability</th>
                            <th>Price/Day</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {props.books.map((book, index )=> (
                            <tr key = {book._id} className="text-center">
                                <td>{index + 1}</td>
                                <td>{book.title}</td>
                                <td>{book.author.join(', ')}</td>
                                <td>{book.availability}</td>
                                <td>{book.price}</td>
                                <td>
                                    <div className="d-flex flex-wrap justify-content-center">
                                        <Button 
                                        onClick = { ()=> props.handleEdit(book)}color="success" className="mx-2">Edit</Button>
                                        <Button onClick = {()=> props.handleDelete(book._id)} color="danger" className="mx-2">Delete</Button>
                                        <Button onClick = { ()=> handleRent(book.price) }>Rent</Button>

                                    </div>

                                </td>
                            </tr>
                        ))}
                    </tbody>
                </Table>
            </div>
            <BookRent
            price = {price} 
            toggleRent = { toggleRent }
            setToggleRent = { ()=> setToggleRent(!toggleRent) }
            />
        </>
    )
}
export default BookCard;