import React, { useState, useEffect } from 'react';
import { Modal, ModalHeader, ModalBody, FormGroup, Label, Input, Dropdown, DropdownToggle, DropdownMenu, DropdownItem, Form, Button } from 'reactstrap';

const BookForm = props => {

    const [openDropdowntypes, setOpenDropdownTypes] = useState(false);
    const [openDropdownGenres, setOpenDropdownGenres] = useState(false);
    const [selectedType, setSelectedType] = useState({});
    const [selectedGenre, setSelectedGenre] = useState({});
    const [selectedPrice, setSelectedPrice] = useState(undefined);
    const [selectedAvailability, setSelectedAvailability] = useState(undefined);
    const [author, setAuthor] = useState([]);
    const [title, setTitle] = useState("");
    const [coverImg, setCoverImg] = useState("");



    useEffect(()=>{
        if(props.isEditing){
            setSelectedType(props.bookToEdit.type);
            setSelectedGenre(props.bookToEdit.genre);
        }else{
            setSelectedGenre({}); 
            setSelectedType({});
        }
    },[props.isEditing, props.bookToEdit])

     //capture image data

     const selectImg = (e) => {
         let image = e.target.files[0];
         console.log(image);
         if(image.name.match(/\.(jpg|JPG|JPEG|jpeg|png|gif)$/)){
            setCoverImg(image)
         }
     }

     const uploadMainImage = async (imageToSave) => {

        // check kung nageedit at kung may laman yung image

        if((props.isEditing && imageToSave) || !props.isEditing){
            const data = new FormData();
            data.append('image', coverImg, imageToSave.name);

            const imageData = await fetch('https://calm-crag-05005.herokuapp.com/upload', {
                method: 'POST',
                body: data
            })

            const imageUrl = await imageData.json();

            return imageUrl;
        }else{
            return "";
        }
        
    }


    const captureData = async () => {

        let mainImg = await uploadMainImage(coverImg);
        console.log(mainImg);
        await props.saveBook(title, author, selectedType._id, selectedGenre._id, mainImg.imgPath, selectedAvailability, selectedPrice);
        setSelectedType({});
        setSelectedGenre({});
        setSelectedPrice(null);
        setSelectedAvailability(null);
        setAuthor("");
        setTitle("");
        setCoverImg("");
    }


    return(
        <Modal
        isOpen = {props.showForm}
        toggle = { ()=> {props.handleToggle()} }
        centered
        scrollable>
            <ModalHeader
            toggle = { ()=> {props.handleToggle(); props.setIsEditing(); props.setBookToEdit();} }>
                <h1>{props.isEditing ? "Edit Item" : "New Book"}</h1>
            </ModalHeader>
            <ModalBody>
                <Form>
                <FormGroup>
                    <Label>Title</Label>
                    <Input defaultValue = {props.isEditing ? props.bookToEdit?.title : ""} onChange = {(e) => setTitle(e.target.value)}/>
                </FormGroup>
                <FormGroup>
                    <Label>Author</Label>
                    <Input 
                    defaultValue = {props.isEditing ? props.bookToEdit?.title : ""}
                    onChange = {(e) => setAuthor(e.target.value)}/>
                </FormGroup>
                <FormGroup>
                    <Label>Type</Label>
                    <Dropdown
                    isOpen = {openDropdowntypes}
                    toggle = {() => setOpenDropdownTypes(!openDropdowntypes)}
                    direction="right">
                        <DropdownToggle caret>
                            {selectedType ? selectedType?.name : "Choose a type"}
                        </DropdownToggle>
                        <DropdownMenu className = "ml-2">
                            { props.types.map(type => (
                                <>
                                <DropdownItem divider />
                                <DropdownItem 
                                onClick = {()=> setSelectedType(type)}
                                key = {type._id}>{type.name}</DropdownItem>
                                </>
                            ))}
                        </DropdownMenu>
                    </Dropdown>
                </FormGroup>
                <FormGroup>
                    <Label>Genre</Label>
                    <Dropdown
                    isOpen = {openDropdownGenres}
                    toggle = {() => setOpenDropdownGenres(!openDropdownGenres)}
                    direction="right"
                    >
                        <DropdownToggle caret>
                            {selectedGenre ? selectedGenre?.name : "Choose a Genre"}
                        </DropdownToggle>
                        <DropdownMenu className = "ml-2"
                        modifiers={{
                            setMaxHeight: {
                              enabled: true,
                              order: 890,
                              fn: (data) => {
                                return {
                                  ...data,
                                  styles: {
                                    ...data.styles,
                                    overflow: 'auto',
                                    maxHeight: '200px',
                                  },
                                };
                              },
                            },
                          }}>
                            { props.genres.map(genre => (
                                <DropdownItem 
                                onClick = {()=> setSelectedGenre(genre)}
                                key = {genre._id}>{genre.name}</DropdownItem>
                            ))}
                        </DropdownMenu>
                    </Dropdown>
                </FormGroup>
                <FormGroup>
                        <Label>Image</Label>
                        <Input type="file" onChange = { selectImg }/>
                </FormGroup>
                <FormGroup>
                    <Label>Price</Label>
                    <Input type="number" 
                    onChange = {(e)=> setSelectedPrice(parseInt(e.target.value))}
                    defaultValue = {props.isEditing ? props.bookToEdit?.price : selectedPrice}/>
                </FormGroup>
                <FormGroup>
                    <Label>Availability</Label>
                    <Input type="number" 
                    onChange = {(e)=> setSelectedAvailability(parseInt(e.target.value))}
                    defaultValue = {props.isEditing ? props.bookToEdit?.availability : selectedAvailability} />
                </FormGroup>
                    <Button onClick = { captureData }>{props.isEditing ? "Update" : "Save"}</Button>
                </Form>
            </ModalBody>
        </Modal>
    )
}

export default BookForm;