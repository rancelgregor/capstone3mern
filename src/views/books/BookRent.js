import React, { useState } from 'react';
import DayPicker, { DateUtils } from 'react-day-picker';
import 'react-day-picker/lib/style.css';
import { Modal, ModalHeader, ModalBody, ModalFooter, Button } from 'reactstrap';
import moment from 'moment';
import { useEffect } from 'react';

const BookRent = props => {

    const [date, setDate] = useState({
        from : undefined,
        to : undefined,
    });

    const [total, setTotal] = useState(undefined);


    useEffect(()=>{
        const diff = moment(date.to ? date.to : date.from).diff(moment(date.from), 'days') + 1;
        setTotal(props.price * diff);
    },[date])

    const handleDayClick = (day, modifiers = {}) => {

        if(!modifiers.disabled){
            const range = DateUtils.addDayToRange(day,date);
            setDate(range);
                
        }
    }

    const handleResetClick = () => {
        setDate({});
    }

    const { from, to } = date;
    const modifiers = { start : from, end: to }
    const modifiersStyles = {
        selected : {
            backgroundColor : "salmon",
            outline: "none"
        },
        disabled : {
            backgroundColor : "gray"
        }
    }


    return(
        <Modal
        isOpen = {props.toggleRent}
        toggle = {props.setToggleRent}>
            <ModalHeader
            toggle = {props.setToggleRent}>
                Select Date
            </ModalHeader>
            <ModalBody
            className = "d-flex flex-column align-items-center w-100">
                <DayPicker 
                className = "Selectable"
                numberOfMonths = { 1 }
                fromMonth = { new Date() }
                selectedDays = {[from, { from, to}]}
                modifiers = { modifiers }
                modifiersStyles = { modifiersStyles }
                onDayClick = { handleDayClick }/>

                <code>Price: {props.price}</code>
                <code>Total Amount: {total ? total : "0"}</code>

            </ModalBody>
            <ModalFooter>
                <Button
                className = "col-md-12"
                onClick = { handleResetClick }>
                    Reset
                </Button>
            </ModalFooter>
        </Modal>
    )
}

export default BookRent;