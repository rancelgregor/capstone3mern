import React, { useState, useEffect } from 'react';
import {Container, Card, CardHeader, CardBody, Button} from 'reactstrap';
import BookForm from './BookForm';
import BookCard from './BookCard';

const Books = () => {

    const [showForm, setShowForm] = useState(false);
    const [types, setTypes] = useState([]);
    const [genres, setGenres] = useState([]);
    const [books, setBooks] = useState([]);
    const [isEditing, setIsEditing] = useState(false);
    const [bookToEdit, setBookToEdit] = useState({});


    useEffect(() => {
        fetch('https://calm-crag-05005.herokuapp.com/admin/books')
        .then(res => res.json())
        .then(res => {
            console.log(res);
            setBooks(res);
        });
    }, []);

    useEffect(() => {
        fetch('https://calm-crag-05005.herokuapp.com/admin/types')
        .then(res => res.json())
        .then(res => {
            console.log(res);
            setTypes(res);
        });
    }, []);

    useEffect(() => {
        fetch('https://calm-crag-05005.herokuapp.com/admin/genres')
        .then(res => res.json())
        .then(res => {
            console.log(res);
            setGenres(res);
        });
    }, []);

    console.log(bookToEdit);
    const saveBook = (
        title, 
        author, 
        bookType, 
        genre, 
        coverImg, 
        availability, 
        price) =>{
        console.log(coverImg);
        if(isEditing){
            let id = bookToEdit._id;
            let editedTitle = title;
            let editedAuthor = author;
            let editedBookType = bookType;
            let editedCoverImg = coverImg;
            let editedGenre = genre;
            let editedAvailability = availability;
            let editedPrice = price;
            
            if(title == "") editedTitle = bookToEdit.title;
            if(author == "") editedAuthor = bookToEdit.author;
            if(bookType == "") editedBookType = bookToEdit.type;
            if(genre === "") editedGenre = bookToEdit.genre;
            if(coverImg === undefined) editedCoverImg = bookToEdit.coverImg;
            if(availability === undefined) editedAvailability = bookToEdit.availability;
            if(price === undefined) editedPrice = bookToEdit.price;
            

            fetch('https://calm-crag-05005.herokuapp.com/admin/updatebook', {
                method: "PATCH",
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    id,
                    title: editedTitle,
                    author: editedAuthor,
                    type: bookToEdit.type,
                    genre: bookToEdit.genre,
                    coverImg: editedCoverImg,
                    availability: editedAvailability,
                    price: editedPrice
                })
            }).then(res => res.json())
            .then(res => { 
                console.log(res);
                let newBooks = books.map( book=> {
                    if(book._id === id){
                        book.title = editedTitle;
                        book.author = editedAuthor;
                        book.type = editedBookType;
                        book.genre = editedGenre;
                        book.coverImg = editedCoverImg;
                        book.availability = editedAvailability;
                        book.price = editedPrice;
                        book.isAvailable = bookToEdit.isAvailable;
                    }
                    return book;
            })
                
                setGenres(newBooks);
            })

            setIsEditing(false);
            setBookToEdit({});

        }else{
            fetch('https://calm-crag-05005.herokuapp.com/admin/addbook', {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    title, 
                    author, 
                    type: bookType, 
                    genre, 
                    coverImg, 
                    availability, 
                    price
                })
            }, []).then(res=>res.json())
            .then(res=>{
                setBooks([...books, res]);
            });
        }
        setShowForm(false);
    }

    const handleDelete = (id) => {

        const apiOptions = {
            method: "DELETE",
            headers: {"Content-Type" : "application/json"},
            body: JSON.stringify({
                id
            })
        }

        fetch('https://calm-crag-05005.herokuapp.com/admin/deletebook', apiOptions)
        .then(res => res.json())
        .then(res => {
            const newBooks = books.filter( book => {
                return book._id !== id
            });

            setBooks(newBooks);
        })
    }

    


    const handleToggle = () => {
        setShowForm(!showForm);
    }


    const handleEdit = (book) =>{
        setBookToEdit(book);
        setIsEditing(true);
        setShowForm(true);
        console.log(bookToEdit);
    }


    console.log(bookToEdit);
    return(
        <React.Fragment>
        <Container>
        <Card className="mt-5">
            <CardHeader>
                <h1 className="text-center">Add a Book</h1>
                <Button
                color = "info"
                onClick = { ()=> {handleToggle(); setIsEditing(false); setBookToEdit({})} }>
                    +Book
                </Button>
            </CardHeader>
            <CardBody>
                <BookCard
                handleDelete = {handleDelete} 
                handleEdit = {handleEdit}
                books = { books }/>
            </CardBody>
        </Card>
        </Container>
        <BookForm
        setBookToEdit = { ()=> setBookToEdit({}) }
        setIsEditing = { ()=> setIsEditing(false) }
        bookToEdit = {bookToEdit}
        isEditing = {isEditing}
        saveBook = {saveBook}
        types = {types}
        genres = {genres}
        showForm = {showForm}
        handleToggle = { handleToggle }/>
        </React.Fragment>
    )
}

export default Books;