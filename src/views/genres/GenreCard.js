import React from 'react';
import {Card, CardHeader, CardBody, CardImg, Row, CardFooter, Button} from 'reactstrap';

const GenreCard = props => {


    return(
        <div>
                    <Row>
                        {props.genres.map((genre) =>(
                        <Card className="col-md-3 offset-md-1"
                            key = {genre._id}>
                            <CardHeader>
                                {genre.name}
                            </CardHeader>
                            <CardImg src = {"https://calm-crag-05005.herokuapp.com/" + genre.imgPath} height = "300px" />
                            <CardBody>
                                {genre.description}
                            </CardBody>
                            <CardFooter>
                                <Button onClick = {()=> props.handleEdit(genre)}>Edit</Button>
                                <Button onClick = {() => props.handleDelete(genre._id)}>Delete</Button>
                            </CardFooter>
                        </Card>
                        ))}
                    </Row>     
        </div>
    )
}

export default GenreCard;