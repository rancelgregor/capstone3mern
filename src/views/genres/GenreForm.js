import React,{useState, useEffect} from 'react'
import {Modal, ModalHeader, ModalBody, Form, FormGroup, Label, Input, Button, Dropdown, DropdownToggle, DropdownMenu, DropdownItem} from 'reactstrap';

const GenreForm = props => {


    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [imgPath, setImgPath] = useState(null);
    const [toggleBtn, setToggleBtn] = useState(false);
    const [bookTypes, setBookTypes] = useState([]);
    const [selected, setSelected] = useState({});
    




        //fetch types
        useEffect(()=>{
            fetch('https://calm-crag-05005.herokuapp.com/admin/types')
            .then(res => res.json())
            .then(res => {
                console.log(res);
                setBookTypes(res);  
            })
        }, []);

    //capture image data
    const selectImg = (e) => {
        let image = e.target.files[0];
        console.log(image);
        if(image.name.match(/\.(jpg|JPG|JPEG|jpeg|png|gif)$/)){
            setImgPath(image);
        }
    }

    const uploadMainImage = async (imageToSave) => {

        // check kung nageedit at kung may laman yung image

        if((props.isEditing && imageToSave) || !props.isEditing){
            const data = new FormData();
            data.append('image', imgPath, imageToSave?.name);

            const imageData = await fetch('https://calm-crag-05005.herokuapp.com/upload', {
                method: 'POST',
                body: data
            })

            const imageUrl = await imageData.json();

            return imageUrl;
        }else{
            return "";
        }
        
    }




    const InitializeGenre = async () =>{

        let mainImg = await uploadMainImage(imgPath)  
        console.log(mainImg);
        props.saveGenre(name, description,mainImg.imgPath,selected._id);
        setName("");
        setDescription("");
        setImgPath("");

    }

    console.log(props.genreToEdit);
    return(
        <Modal
        isOpen = {props.showForm}
        toggle = {props.toggleForm}
        centered
        scrollable>
            <ModalHeader className="col-md-12"
            toggle = {props.toggleForm} >
                    {props.isEditing ? `Edit ${props.genreToEdit?.name}` : "Add a new genre"}
            </ModalHeader>
            <ModalBody>
                <Form>
                    <FormGroup>
                        <Label className="text-center">Genre Name</Label>
                        <Input name="name" type="text" defaultValue = {props.genreToEdit?.name}
                        onChange = { (e) => setName(e.target.value)}/>
                    </FormGroup>
                    <FormGroup>
                        <Label className="text-center">Description</Label>
                        <Input name="description" type="text" defaultValue = {props.genreToEdit?.description}
                        onChange = {(e) => setDescription(e.target.value)}/>
                    </FormGroup>

                        <Dropdown isOpen = {toggleBtn} toggle = {()=> setToggleBtn(!toggleBtn)}>
                            <DropdownToggle caret>{props.isEditing ? props.genreToEdit?.type?.name : (selected?.name ? selected?.name : "Choose a Type")}</DropdownToggle>
                            <DropdownMenu>
                                {bookTypes.map( bookType => (
                                    <DropdownItem onClick = {() => setSelected(bookType)} key = {bookType._id}>{bookType.name}</DropdownItem>
                                ))}
                            </DropdownMenu>
                        </Dropdown>


                    <FormGroup>
                        <Label>Image File</Label>
                        <Input type="file" name="imgPath" onChange={selectImg}></Input>
                    </FormGroup>
                    <Button className="col-md-12"
                    onClick = {InitializeGenre}>{props.isEditing ? "Edit" : "Submit"}</Button>
                </Form>
            </ModalBody>
        </Modal>
    )
}

export default GenreForm;