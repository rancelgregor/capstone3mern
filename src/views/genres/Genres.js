import React,{useState, useEffect} from 'react';
import { Container, Col, Row, Card, CardHeader, CardBody, Button } from 'reactstrap';
import GenreForm from './GenreForm';
import GenreCard from './GenreCard';

const Genres = () => {

    const [showForm, setShowForm] = useState(false);
    const [genres, setGenres] = useState([]);
    const [isEditing, setIsEditing] = useState(false);
    const [genreToEdit, setGenreToEdit] = useState({});

    //fetch genres
    useEffect(()=>{
        fetch('https://calm-crag-05005.herokuapp.com/admin/genres')
        .then(res => res.json())
        .then(res => {
            setGenres(res);
        })
    },[]);

    //saving Genre
    const saveGenre = (name,description,imgPath, type) =>{
        console.log(genreToEdit);
        
        if(isEditing){
           console.log(imgPath);
            let id = genreToEdit._id;
            let editedName = name;
            let editedDescription = description;
            let editedImgPath = imgPath;
            let editedType = type;
            
            if(name == "") editedName = genreToEdit.name;
            console.log(isEditing);
            if(description === "") editedDescription = genreToEdit.description;
            if(imgPath === "") editedImgPath = genreToEdit.imgPath;
            if(type === "") editedType = genreToEdit.type._id;
            

            fetch('https://calm-crag-05005.herokuapp.com/admin/updateGenre', {
                method: "PATCH",
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    id,
                    name: editedName,
                    description: editedDescription,
                    imgPath: editedImgPath,
                    type: editedType
                })
            }).then(res => res.json())
            .then(res => { 
                console.log(res);
                let newGenres = genres.map( genre=> {
                    if(genre._id === id){
                        genre.name = editedName;
                        genre.description = editedDescription;
                        genre.imgPath = editedImgPath;
                        genre.type = editedType
                    }
                    return genre;
            })
                
                setGenres(newGenres);
            })

            setIsEditing(false);
            setGenreToEdit(false);

        }else{
            fetch('https://calm-crag-05005.herokuapp.com/admin/addGenre', {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    name,
                    description,
                    imgPath,
                    type
                })
            }, []).then(res=>res.json())
            .then(res=>{
                setGenres([...genres, res]);
            });
        }
        setShowForm(false);
    }


    const toggleForm = () =>{
        setShowForm(!showForm);
        setIsEditing(false);
        setGenreToEdit({});
    }


    const handleEdit = (genre) =>{
        setGenreToEdit(genre);
        setIsEditing(true);
        setShowForm(true);
    }


    const handleDelete = (id) => {

        const apiOptions = {
            method: "DELETE",
            headers: {"Content-Type" : "application/json"},
            body: JSON.stringify({
                id
            })
        }

        fetch('https://calm-crag-05005.herokuapp.com/admin/deleteGenre', apiOptions)
        .then(res => res.json())
        .then(res => {
            const newGenre = genres.filter( genre => {
                return genre._id !== id
            });

            setGenres(newGenre);
        })
    }

    return(
       <React.Fragment>
           <Container>
               <Card className ="mt-5">
                   <CardHeader>
                       <Row>
                           <Col className="md-12 h1 text-center">Book Genres</Col>
                       </Row>
                       <Button
                       onClick = {()=> setShowForm(true)}>
                           + Add New Genre
                       </Button>
                   </CardHeader>
                   <CardBody>
                       <GenreCard
                       handleDelete = { handleDelete }
                       handleEdit = {handleEdit} 
                       genres = {genres}
                       toggleForm = {toggleForm}
                       />
                   </CardBody>
               </Card>
           </Container>
            <GenreForm
            genreToEdit = {genreToEdit} 
            isEditing = {isEditing}
            showForm = {showForm}
            toggleForm = {toggleForm}
            saveGenre = {saveGenre} />
       </React.Fragment>
    )
}

export default Genres;