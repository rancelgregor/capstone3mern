import React, {useState} from 'react';
import { Form, FormGroup, Input, Label, Button } from 'reactstrap';
import Background from '../../bookRegister.png';
import Library from '../../libraryRegister.jpg';


const Login = props => {

    const [ email, setEmail ] = useState('');
    const [ password, setPassword ] = useState('');

    const handleLogin = async() => {
        const apiOptions = {
            method: "POST",
            headers: {'Content-type': 'application/json'},
            body: JSON.stringify({
                email,
                password
            })
        }

        const fetchedData = await fetch('https://calm-crag-05005.herokuapp.com/login', apiOptions);
        const data = await fetchedData.json();
        // console.log(data);

        sessionStorage.token = data.token;
        sessionStorage.userId = data.user.id;
        sessionStorage.isAdmin = data.user.isAdmin;
        sessionStorage.userName = data.user.firstName + " " + data.user.lastName;
        sessionStorage.email = data.user.email;

        window.location.replace('/books');
    }

    return(
        <React.Fragment>

        
        <div className = "d-flex justify-content-center align-items-center vh-100 border border-danger vw-100" 
        style = {{backgroundImage: `url(${Library})`, backgroundRepeat: "no-repeat", backgroundSize: "cover"}}>
            <div className = "d-flex" style = {{
                borderTopLeftRadius: "20px",
                height: "70%",
                width: "60%"
            }}>
                <div className = "border-right-0" style = {{
                    border: "2px solid gray",
                    borderTopLeftRadius: "20px",
                    borderBottomLeftRadius: "20px",
                    backgroundColor: "rgba(214,210,204,0.5970763305322129)",
                    width: "35%"
                }}>
                    <Form className = "d-flex flex-column justify-content-center align-items-center" style = {{height: "100%", width: "100%"}}>
                    <FormGroup style = {{width: "70%"}}>
                        <Label className = "h5">Email:</Label>
                        <Input 
                            type = 'email'
                            placeholder = 'Enter your email'
                            onChange = { (e) => setEmail(e.target.value) }
                        />
                    </FormGroup>
                    <FormGroup style = {{width: "70%"}}>
                        <Label className = "h5">Password:</Label>
                        <Input 
                            type='password'
                            placeholder = 'Enter your password'
                            onChange = { (e) => setPassword(e.target.value) }
                        />
                    </FormGroup>
                    <Button 
                        disabled = { email === '' || password === '' ? true : false}
                        color='warning'
                        onClick = { handleLogin }
                    >Login</Button>
                    </Form>

                </div>
                <div className = "border-left-0 d-flex flex-column align-items-center justify-content-around text-white"style = {{
                    backgroundColor: "rgba(191,142,67,0.4066001400560224)",
                    border: "2px solid gray",
                    width: "65%"
                }}>
                    <h1>Login</h1>
                    <img src = {Background}/>
                </div>
            </div>
        </div>

        </React.Fragment>
    )
}

export default Login;