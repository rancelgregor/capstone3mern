import React, {useState} from 'react';
import { Form, FormGroup, Input, Label, Button } from 'reactstrap';
import Background from '../../bookRegister.png';
import Library from '../../libraryRegister.jpg';


const Register = props => {

    const [ firstName, setFirstName ] = useState('');
    const [ lastName, setLastName ] = useState('');
    const [ email, setEmail ] = useState('');
    const [ password, setPassword ] = useState('');
    const [ confirmPassword, setConfirmPassword ] = useState('');

    const handleRegister = async () => {
        const apiOptions = {
            method: "POST",
            headers: {'Content-type': 'application/json'},
            body: JSON.stringify({
                firstName,
                lastName,
                email,
                password
            })
        }

        await fetch('https://calm-crag-05005.herokuapp.com/register', apiOptions);

        window.location.replace('/login');
    } 

    return(
        <React.Fragment>

        
        <div className = "d-flex justify-content-center align-items-center vh-100 border border-danger vw-100" 
        style = {{backgroundImage: `url(${Library})`, backgroundRepeat: "no-repeat", backgroundSize: "cover"}}>
            <div className = "d-flex" style = {{
                borderTopLeftRadius: "20px",
                height: "70%",
                width: "60%"
            }}>
                <div className = "border-right-0" style = {{
                    border: "2px solid gray",
                    borderTopLeftRadius: "20px",
                    borderBottomLeftRadius: "20px",
                    backgroundColor: "rgba(214,210,204,0.5970763305322129)",
                    width: "35%"
                }}>
                    <Form className = "d-flex flex-column justify-content-center align-items-center" style = {{height: "100%", width: "100%"}}>
                        <FormGroup style = {{width: "70%"}}>
                            <Label className="h5">First Name</Label>
                            <Input 
                            type = "text"
                            placeholder = "Please enter you first name"
                            onChange = {(e) => setFirstName(e.target.value)}/>
                        </FormGroup>
                        <FormGroup style = {{width: "70%"}}>
                            <Label className="h5">Last Name</Label>
                            <Input 
                            type = "text"
                            placeholder = "Please enter you first name"
                            onChange = {(e) => setLastName(e.target.value)}/>
                        </FormGroup>
                        <FormGroup style = {{width: "70%"}}>
                            <Label className="h5">E-Mail</Label>
                            <Input 
                            type = "email"
                            placeholder = "Please enter you first name"
                            onChange = {(e) => setEmail(e.target.value)}/>
                        </FormGroup>
                        <FormGroup style = {{width: "70%"}}>
                            <Label className="h5">Password</Label>
                            <Input 
                            type = "password"
                            placeholder = "Please enter you first name"
                            onChange = {(e) => setPassword(e.target.value)}/>
                            <span className='text-danger'>{ confirmPassword !== '' && confirmPassword !== password ? 'Passwords did not match' : ''}</span>
                        </FormGroup>
                        <FormGroup style = {{width: "70%"}}>
                            <Label className="h5">Confirm Password</Label>
                            <Input 
                            type = "password"
                            placeholder = "Please enter you first name"
                            onChange = {(e) => setConfirmPassword(e.target.value)}/>
                        </FormGroup>
                        <Button
                        type='button'
                        color='warning'
                        disabled = { firstName === '' || lastName === '' || email === '' || password === '' || password !== confirmPassword ? true : false }
                        onClick = { handleRegister }
                    >
                        Register
                    </Button>
                    </Form>

                </div>
                <div className = "border-left-0 d-flex flex-column align-items-center justify-content-around text-white"style = {{
                    backgroundColor: "rgba(191,142,67,0.4066001400560224)",
                    border: "2px solid gray",
                    width: "65%"
                }}>
                    <h1>Registration</h1>
                    <img src = {Background}/>
                </div>
            </div>
        </div>

        </React.Fragment>
    )
}

export default Register;