// import React, {useState} from 'react';
// import { Container, Row, Card, CardBody, CardHeader } from 'reactstrap';
// import { useEffect } from 'react';
// const fictionImg = require('../fiction.jpeg');

// const Types = props => {

//     const [types, setTypes] = useState([]);


//     useEffect(()=> {
//         fetch('http://localhost:4000/admin/types')
//         .then(res => res.json())
//         .then(res => {
//             console.log(res);
//             setTypes(res);
//         })
//     }, [])



//     return(
//         <Container>
//             <Row>
//                 <Card className="col-md-12 my-5">
//                     <CardHeader className="my-3 bg-primary">
//                         <h1 className="text-center">Choose a type of Book</h1>
//                     </CardHeader>
//                     <CardBody>
//                         <div className="d-flex justify-content-around">
//                             {types.map((type)=>(
//                                 <Card key = {type._id} className="col-md-4">
//                                     <CardHeader>
//                                         <h1 className="text-center">{type.name}</h1>
//                                     </CardHeader>
//                                     <CardBody>
//                                         <img src = {fictionImg} alt="" style = {{height = "100%", width = "100%"}}/>
//                                     </CardBody>
//                                 </Card>
//                             ))}
//                         </div>
//                     </CardBody>
//                 </Card>
//             </Row>
//         </Container>
//     )
// }

// export default Types;